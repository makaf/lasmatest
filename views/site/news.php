<?php
use yii\helpers\Html;


file_put_contents($_SERVER["SERVER_ROOT"].'/temp/news.xml', file_get_contents('https://www.delfi.lv/rss/?channel=auto'));
$xml = simplexml_load_file($_SERVER["SERVER_ROOT"].'/temp/news.xml');
foreach ($xml->channel->item as $item)
{
	echo '<div style="overflow: hidden;">';
	echo '<a href="'.$item->link.'" target="_blank"><h2>'.$item->title.'</h2></a>';
	echo '<p>'.date_format(date_create($item->pubDate),"d.m.Y H:i").'</p>';
	echo '<img style="float: left;padding-right:10px;" src="'.$item->children('media',true)->attributes()->url.'" >';
	echo '<br>';
	echo '<div>'.$item->description.'</div>';
	echo '</div>';
}
?>