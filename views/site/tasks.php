<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
?>

<?php 
if($accessLevel>=1) 
{
	$form = ActiveForm::begin(['action' => ['site/addtask']]);
    echo $form->field($model, 'title');
    echo $form->field($model, 'type');
    echo $form->field($model, 'description');
    echo $form->field($model, 'imageFile')->fileInput();
    echo '<div class="form-group">';
    echo Html::submitButton('Add task', ['class' => 'btn btn-primary']);
    echo '</div>';
	ActiveForm::end();
}
foreach ($model->getModel() as $task) 
{
	echo '<div style="overflow: hidden;">';
	echo '<h2>'.$task->title.'</h2>';

	echo '<h3>'.$task->type.'</h3>';
	echo '<img style="float: left;padding-right:10px;max-width:100px;" src="'.$_SERVER["SERVER_ROOT"].'/images/'.$task->img.'" >';
	echo '<br>';
	echo '<div>'.$task->description.'</div>';
	if($accessLevel>=1) 
	{
		echo '<br>';
		echo '<a href="'.$_SERVER["SERVER_ROOT"].'/index.php?r=site%2Fdeletetask&id='.$task->id.'">Delete</a>';
	}
	echo '</div>';
}
?>


