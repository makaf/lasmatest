<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Sveiki!</h1>

        <p class="lead">Šeit reģistrētie lietotāji var apskatīt ziņas :)</p>

        <p><a class="btn btn-lg btn-success" href="/index.php?r=site%2Fnews">Uz ziņām</a></p>
    </div>
</div>
