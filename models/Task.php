<?php

namespace app\models;

use yii\db\ActiveRecord;

class Task extends ActiveRecord
{
	public $imageFile;
	
	public static function tableName()
    {
         return 'task';
    }
	
    public static function getModel()
    {
		$model = Task::find()->orderBy("id")->all();  
		return $model;
    }
	public static function deleteTaskByID($id)
    {
		$model = Task::findOne($id);
		@unlink ($_SERVER["DOCUMENT_ROOT"].'/images/'.$model->img);
		return $model->delete();
		
    }
	
	   public function rules()
    {
        return [
            [['title', 'type','description'], 'required'],
			[['imageFile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg']
        ];
    }
	
	public function upload()
    {
        $this->imageFile->saveAs($_SERVER["DOCUMENT_ROOT"].'/images/'.$this->imageFile->baseName . '.' . $this->imageFile->extension);
    }
}