<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Task;
use app\models\TaskForm;
use yii\web\UploadedFile;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
			'access' => [
                'class' => AccessControl::className(),
                'only' => ['tasks','news'],
                'rules' => [
                    [
                        'actions' => ['tasks','news'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
	
	public function actionNews()
    {
        return $this->render('news');
    }
	
	public function actionTasks()
    {
		$model = new Task();
		$accessLevel = Yii::$app->user->identity->level;
        return $this->render('tasks', [
            'model' => $model,
			'accessLevel' => $accessLevel
        ]);
    }
    public function actionAddtask()
    {
		$model = new Task();
		$request_data = Yii::$app->request->post()['Task'];
        $model->title = $request_data['title'];
        $model->type = $request_data['type'];
        $model->description = $request_data['description'];
        $model->imageFile = UploadedFile::getInstance($model,'imageFile');
        if($model->imageFile)
		{
			$model->img = $model->imageFile->baseName . '.' . $model->imageFile->extension;
        }
        $model->save();
        if($model->imageFile)
		{
            $model->upload();
        }
        Yii::$app->getSession()->setFlash('success', 'Your message has been successfully recorded');
		return $this->redirect(['site/tasks']);	
    }
	public function actionDeletetask($id)
    {
		$model = new Task();
		$accessLevel = Yii::$app->user->identity->level;
		if($accessLevel>=1)
		{
			if($model->deleteTaskByID($id))
				Yii::$app->getSession()->setFlash('success', 'Task deleted');
			else
				Yii::$app->getSession()->setFlash('error', 'Delete failed');
		}
        return $this->redirect(['site/tasks']);	
    }
}
